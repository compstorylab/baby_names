# Baby Names

Project for analyzing cultural dynamics through the Social Security Administrations baby names dataset.

Features include scripts for plotting baby name popularity by state and embedding names by time series similarity.

## Installation
To setup a conda environment with necessary dependencies, clone the repo, navigate to the project directory and run

`conda create -n <environment-name> --file requirements.txt`

## Usage

### Baby Name Maps

To create all years of baby name maps, run the baby_name_maps.py script in `src`:

`python baby_name_maps.py -n Lou -s M`,

where `-n` is for name and `-s` is for sex, either 'M' or 'F' matching the SSA columns. This will also produce an `*.mp4` movie.

To produce a map for just one year, specify it with `-y`:

`python baby_name_maps.py -n Lou -s M -y 1930`.
