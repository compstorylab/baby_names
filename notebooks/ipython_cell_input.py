
def state_grid_plot(state_dict, df, name, sex, year, colormap, h_space=0.15, v_space=0.15):
    """ """
    height = 1-v_space
    width = 1-h_space
    k=2
    f,ax = plt.subplots(figsize=(7,5),dpi=200)
    norm = Normalize(vmin=0.0, vmax=df[df['name']==name]['count'].max())
    
    sub_df = df[(df['name']==name)&(df['year']==int(year))&(df['sex']==sex)][['state','count']]
    data = sub_df.set_index('state').to_dict()['count']
    
    for state, state_coords in state_dict.items():
        
        count = df[(df['name']=='Michael')&(df['year']==year)&(df['sex']=='M')]['count']
        state_coords_b = (state_coords[0]+0.05,state_coords[1]-0.05)
        ax.add_patch(Rectangle(state_coords_b,
                               height=height, width=width,
                               facecolor='k', alpha=0.2, zorder=0))
        ax.add_patch(Rectangle(state_coords, height=height,
                               width=width,
                               facecolor='w', alpha=1, zorder=0))
        ax.add_patch(Rectangle(state_coords,
                               height=height, width=width,
                               facecolor=colormap(norm(data[state])), alpha=0.5))
        ax.text(state_coords[0]+width/k, state_coords[1]+(height-v_space/2)/k, state,
                horizontalalignment='center',
                verticalalignment='center',
                fontname='Helvetica',
                color='#333333',
                fontsize=8.5)
    ax.set_ylim(-1,10)
    ax.set_xlim(-1,10)
    ax.axis('equal')
    
    plt.axis('off')
    return f, ax
    
cmap = CMA.get_cmap('inferno')
f,ax = state_grid_plot(state_dict,df,"Michael",'M','2002',cmap)
