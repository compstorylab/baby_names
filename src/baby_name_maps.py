import os
import json
import argparse
import subprocess
from matplotlib.patches import Rectangle
from matplotlib.colors import Normalize, LogNorm
from matplotlib import cm
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob
from tqdm import tqdm

def parse_args():
    """ Parse args"""
    parser = argparse.ArgumentParser(description='Create baby name rank maps')
    parser.add_argument('-n','--name',
                        type=str,
                       )
    parser.add_argument('-s','--sex',
                        type=str,
                        choices=['F', 'M'],
                       )
    parser.add_argument('-y','--year',
                        type=int,
                        help="plot only a specific year if given, else plot all")
    parser.add_argument('-c', '--colormap',
                        type=str,
                        default='inferno_r',
                        help="colormap to use, try 'inferno_r', 'viridis_r', 'magma_r', etc.")
    return parser.parse_args()


def state_grid_plot(state_dict, df, name, sex, year, cmap, h_space=0.15, v_space=0.15):
    """ """
    height = 1 - v_space
    width = 1 - h_space
    k = 2
    f, ax = plt.subplots(figsize=(7, 5), dpi=200)
    max_rank = df[df['name'] == name]['rank'].max()
    norm = LogNorm(vmin=1, vmax=3000)
    sub_df = df[(df['name'] == name) & (df['year'] == int(year)) & (df['sex'] == sex)]
    data = sub_df.set_index('state').to_dict()['rank']
    count = sub_df.set_index('state').to_dict()['count']

    for state, state_coords in state_dict.items():

        state_coords_b = (state_coords[0] + 0.05, state_coords[1] - 0.05)
        ax.add_patch(Rectangle(state_coords_b,
                               height=height, width=width,
                               facecolor='k', alpha=0.2, zorder=0))
        ax.add_patch(Rectangle(state_coords, height=height,
                               width=width,
                               facecolor='w', alpha=1, zorder=0))
        try:
            cax = ax.add_patch(Rectangle(state_coords,
                                         height=height, width=width,
                                         facecolor=cmap(norm(data[state])), alpha=0.6))
            ax.text(state_coords[0] + width / k, state_coords[1] + (height - v_space / 2) / k / 3, f"r: {data[state]:.0f}",
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontname='Helvetica',
                    color='#333333',
                    fontsize=4.5)

            ax.text(state_coords[0] + width / k, state_coords[1] + (height - v_space / 2) * 0.88, f"n: {count[state]:.0f}",
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontname='Helvetica',
                    color='#333333',
                    fontsize=4.5)
        except KeyError:
            ax.add_patch(Rectangle(state_coords,
                                   height=height, width=width,
                                   facecolor=cmap(norm(max_rank)), alpha=0.5,
                                   hatch='///', edgecolor='w'))
            ax.text(state_coords[0] + width / k, state_coords[1] + (height - v_space / 2) / k / 3,
                    f"r: N/A",
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontname='Helvetica',
                    color='#333333',
                    alpha=0.4,
                    fontsize=4.5)

            ax.text(state_coords[0] + width / k, state_coords[1] + (height - v_space / 2) * 0.88,
                    f"n: <5",
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontname='Helvetica',
                    color='#333333',
                    alpha=0.4,
                    fontsize=4.5)
        ax.text(state_coords[0] + width / k, state_coords[1] + (height - v_space / 2) / k, state,
                horizontalalignment='center',
                verticalalignment='center',
                fontname='Helvetica',
                color='#333333',
                fontsize=8.5)

    ax.set_ylim(-1, 10)
    ax.set_xlim(-1, 10)
    ax.axis('equal')

    plt.axis('off')
    cbar = plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap),
                        orientation='horizontal',
                        alpha=0.6,
                        fraction=0.046,
                        pad=0.04)
    cbar.ax.set_xlabel('Rank', rotation=0)
    cbar.ax.set_xticks([1, 10, 100, 1000])
    cbar.ax.set_xticklabels([1, 10, 100, 1000])
    cbar.outline.set_visible(False)
    ax.set_title(f"{name} ({sex}): {year}",
                 fontname='Helvetica',
                 color='#333333')
    return f, ax


def create_maps_all_years(df, name, sex, cmap, state_dict):
    """Create name maps for animation"""
    pth = f'../figures/{name}'
    if not os.path.isdir(pth):
        os.mkdir(pth)
    sub_df = df[(df['name'] == name) & (df['sex'] == sex)]

    for year in tqdm(range(1910, 2021)):
        f, ax = state_grid_plot(state_dict, sub_df, name, sex, year, cmap)

        fname = f"../figures/{name}/{name}_{year}_{sex}_name_rank.png"
        plt.savefig(fname)
        plt.close()


def get_state_rank(df, sex):
    """ Return tied ranks with """
    ranks = []

    for year in df[df['sex'] == sex]['year'].unique():
        df_i = df[(df['year'] == year) & (df['sex'] == sex)]
        rank = df_i.rank(method='min', ascending=False)['count'].values.astype(int)
        ranks.extend(rank)

    return ranks


def clean_states():
    """ Load babynames by state"""
    dfs = []
    
    fnames = glob("../data/namesbystate/*.TXT")
    names = ['state', 'sex', 'year', 'name', 'count']

    for fname in fnames:
        df_i = pd.read_csv(fname, names=names)
        df_i['rank'] = get_state_rank(df_i) 
        dfs.append(df_i)
        break
    df = pd.concat(dfs)

    df.to_parquet("../data/states_baby_names_merged.parquet.snappy")
    return df


def load_data():
    "Load all data files, including name data"

    # load state grid positions data
    with open('../data/state_grid.json', 'r', encoding='utf-8') as f:
        state_dict = json.load(f)

    # load data file
    fname = f"../data/states_baby_names_merged.parquet.snappy"
    if os.path.isfile(fname):
        df = pd.read_parquet(fname)
    else:
        df = clean_states()

    return state_dict, df



def main(args=None):
    """ Create maps and animated maps of baby name popularity by name and sex"""
    if args is None:
        args = parse_args()

    state_dict, df = load_data()

    cmap = cm.get_cmap(args.colormap)

    if args.year:
        state_grid_plot(state_dict, df, args.name, args.sex, args.year, cmap)
        plt.show()
    else:
        create_maps_all_years(df, args.name, args.sex, cmap, state_dict)
        subprocess.call(f"bash convert_to_mp4.sh {args.name} {args.sex}", shell=True)


if __name__ == "__main__":
    main()

