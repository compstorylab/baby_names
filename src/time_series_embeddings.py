import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import umap
import time

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.cluster import MiniBatchKMeans

import datetime
import matplotlib.dates as mdates

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import glob
import collections
import scipy.stats as stats
import matplotlib.dates as mdates

from bokeh.models import CustomJS, CrosshairTool
from bokeh.layouts import column
import bokeh
from bokeh.models.widgets import TextInput
from bokeh.models import ColorBar, LogColorMapper, LinearColorMapper, CategoricalColorMapper, CDSView, GroupFilter, BooleanFilter
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.io import show
from bokeh.models import CustomJS, RadioButtonGroup, OpenURL, TapTool, Panel, Tabs, Select, TextInput, DateRangeSlider, DateSlider
from bokeh.palettes import Viridis5, Category20


font = {'family' : 'serif',
        'weight' : 'normal',
        'size' : 13}
plt.rc('font', **font)


def get_unique_names(name_files):

    unique = set()
    for i in name_files:
        x = pd.read_csv(i, names=['name', 'sex', 'count'])
        x['name'] = x['name'] + '_' + x['sex']
        unique.update(x['name'].values)
    return list(unique)


def load_us_data():
    """ Load all required data, norm_array for total counts, """
    norm_array = pd.read_csv('../data/names/name_counts.dat',
                             names=['year', 'male', 'female', 'count'],
                             delim_whitespace=True,
                             header=None,
                             )
    start_year = 1880
    end_year = 2022
    name_files = [f'../data/names/yob{i}.txt' for i in range(start_year, end_year)]
    years = [str(i) for i in range(start_year, end_year)]
    unique = get_unique_names(name_files)

    datafile = "../data/names/us_names_timeseries.parquet.snappy"
    # try to read cached file
    if os.path.isfile(datafile):
        df = pd.read_parquet(datafile)

    # or load each year to make on dataframe and save
    else:
        # init empty dict
        data_dict = {name: {year: 0} for name in unique for year in years}
        for i, name in enumerate(name_files):
            print(i+1880, end=' ')
            x = pd.read_csv(name, names=['name', 'sex', 'count'])
            x['name'] = x['name'] + '_' + x['sex']
            for j, row in x.iterrows():
                data_dict[row['name']][name[-8:-4]] = int(row['count'])
        df = pd.DataFrame().from_dict(data_dict, orient='index')
        df.to_parquet(datafile)

    df.fillna(0, inplace=True)
    columns = sorted(list(df.columns.values))
    df = df[columns]
    return norm_array, df, years, unique

def plot_sparklines(df, dates):
    """ Plot sparklines for interactive plots"""
    i=0
    df['imgs'] = [f"../figures/{len(df)}/baby_{name}.png" for name in df.index.values]
    print(df.loc[:, ~df.columns.isin(['imgs'])])
    dates = pd.date_range(start=datetime.datetime(1880, 1, 1), end=datetime.datetime(2022, 1, 1), freq='Y')
    for name, x in df.loc[:, ~df.columns.isin(['imgs'])].astype(int).iterrows():
        f, ax = plt.subplots(figsize=(5, 1.3))
        plt.plot(dates, np.log10(x+1), '.', ms=2)
        ax.axes.get_yaxis().set_visible(False)
        plt.ylim(-0.5, 6)
        locator = mdates.YearLocator(20)
        fmt = mdates.DateFormatter('%Y')
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(fmt)
        sides = ['left', 'right', 'top']
        for side in sides:
            ax.spines[side].set_visible(False)
        plt.tight_layout()
        plt.savefig(f'../figures/{len(df)}/baby_{name}.png',dpi=50)
        print(i,x)
        if i % 1000 == 0:
            print(i)
            plt.show()
        plt.close()
        i += 1

def umap_transform(df):

    print('Start embedding...')
    start_time = time.time()
    reducer = umap.UMAP(densmap=False)
    scaled_data = StandardScaler().fit_transform(df.values)
    embedding = reducer.fit_transform(scaled_data)
    print(f'Embedding completed in: {time.time()-start_time}')
    #sizes = np.sqrt(df.T.mean()+1)/5
    df['x'] = embedding[:, 0]
    df['y'] = embedding[:, 1]
    return df


def interactive_plot(df, outfile):
    """ Plot interactive UMAP embedding"""
    output_file(outfile)

    # todo: add rank, mean, peak year,
    print('ok')
    years = [str(i) for i in range(1880,2022)]

    df['Mean Count'] = df[years].T.mean()
    df['sizes'] = np.log2(df[years].T.mean() + 1)
    df['imgs'] = [f"../figures/{int(len(df))}/baby_{name}.png" for name in df.index.values]
    df['names'] = df.index.str.slice(start=0, stop=-2)
    df['sex'] = df.index.str.slice(start=-1)
    df['Sex'] = [-1 if sex == 'M' else 1 for sex in df['sex'].values ]
    df['Peak Year'] = df[years].T.idxmax()

    color_fields = ['Mean Count', 'Sex', 'Peak Year']
    color_mappers = []

    # add mean count colors
    color_mappers.append(
                        LogColorMapper(palette="Viridis256",
                                       low=min(df['Mean Count']),
                                       high=max(df['Mean Count'])
                                       )
                         )
    # todo: add sex color
    color_mappers.append(
        LinearColorMapper(palette="RdBu3",
                       low=-1,
                       high=1
                       )
    )

    # todo: add peak year color
    color_mappers.append(
        LogColorMapper(palette="Viridis256",
                       low=int(min(df['Peak Year'])),
                       high=int(max(df['Peak Year']))
                       )
    )



    source = ColumnDataSource(df)
    print(df)
    TOOLTIPS = """
            <div>
                <div>
                    <span style="font-size: 12px;">Name:</span>
                    <span style="font-size: 12px;">@names (@sex)</span>
                </div>
                <div>
                <img
                    src="@imgs" height="50" alt="@imgs" width="250"
                    style="float: left; margin: 0px 15px 15px 0px;"
                    border="1"
                ></img>
                </div>
                <div>
                </div>
            </div>
        """
    figure_args = {
        'plot_width': 700, 'plot_height': 700,
        'tooltips': TOOLTIPS,
        'tools': "pan,wheel_zoom,box_zoom,reset,tap",
        'toolbar_location': "below",
        'title': f'Baby Names: 1880-2021',
    }
    tabs_list = []
    panels = []

    for i, color_field in enumerate(color_fields):
        tabs_list.append(figure(plot_width=700, plot_height=700,
                                tooltips=TOOLTIPS,
                                tools="pan,wheel_zoom,box_zoom,reset,tap",
                                toolbar_location="below",
                                title=f'Baby Names: 1880-2021 (Top {len(df)})',
                                ))
        tabs_list[i].circle('x', 'y', size='sizes', source=source,
                                  fill_color={'field': color_field, 'transform': color_mappers[i]},
                                  fill_alpha=0.7, line_color=None)
        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i], title=color_fields[i]))
    show(Tabs(tabs=panels))


def main():

    norm_array, df, years, unique = load_us_data()

    # todo: create figures

    # subset
    df1 = df[df.T.mean().T > 5]
    print(df1.max().max())
    pth = f'../figures/{len(df1)}'
    print(years)
    if not os.path.isdir(pth):
        os.mkdir(pth)
        plot_sparklines(df1, dates=years)

    df1 = umap_transform(df1)



    interactive_plot(df1, 'baby_names.html')


    return df1

if __name__ == "__main__":
    df = main()
